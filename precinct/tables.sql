
--
-- Sector / ภาค
--

CREATE TABLE IF NOT EXISTS `precinct_sector` (
  `id` int NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `name_th` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Province / จังหวัด
--

CREATE TABLE IF NOT EXISTS `precinct_province` (
  `id` int NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `name_th` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `sector_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2b4be5547ec067eb3c007711d31` (`sector_id`),
  CONSTRAINT `FK_2b4be5547ec067eb3c007711d31` FOREIGN KEY (`sector_id`) REFERENCES `precinct_sector` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- District / เขต-อำเภอ
--

CREATE TABLE IF NOT EXISTS `precinct_district` (
  `id` int NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `name_th` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `province_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_260f747a04ef079057245533bec` (`province_id`),
  CONSTRAINT `FK_260f747a04ef079057245533bec` FOREIGN KEY (`province_id`) REFERENCES `precinct_province` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Canton / แขวง-ตำบล
--

CREATE TABLE IF NOT EXISTS `precinct_canton` (
  `id` int NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `name_th` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `district_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d786b94eaee04b23c3561f86b51` (`district_id`),
  CONSTRAINT `FK_d786b94eaee04b23c3561f86b51` FOREIGN KEY (`district_id`) REFERENCES `precinct_district` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- ZIP / รหัสไปรษณีย์
--

CREATE TABLE IF NOT EXISTS `precinct_zip` (
  `id` int NOT NULL AUTO_INCREMENT,
  `zip` varchar(5) NOT NULL,
  `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `canton_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2a53c4f8214b77519bec924c1d6` (`canton_id`),
  CONSTRAINT `FK_2a53c4f8214b77519bec924c1d6` FOREIGN KEY (`canton_id`) REFERENCES `precinct_canton` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17638 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

