INSERT INTO `precinct_sector` (`id`, `name_en`, `name_th`) VALUES
	(1, 'Center', 'ภาคกลาง'),
	(2, 'Northern', 'ภาคเหนือ'),
	(3, 'Eastern', 'ภาคตะวันออก'),
	(4, 'Northeastern', 'ภาคตะวันออกเฉียงเหนือ'),
	(5, 'Western', 'ภาคตะวันตก'),
	(6, 'Southern', 'ภาคใต้');
