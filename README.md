# shared-data

Common data that can be share for many projects.

## Precinct Data

Provides Thai data for provinces (จังหวัด), districts (อำเภอ), canton (ตำบล) and postal codes (รหัสไปรษณีย์).

Thank ThepExcel for collector of these data. This is version 3, dated 2021-11-07.

* https://www.thepexcel.com/thailand-tambon-database/
* https://github.com/ThepExcel/

This data can be used FREELY. :)

